const selectTag = document.getElementById("conference");

window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/conferences/";
    try {
        const response = await fetch(url);
        if (!response.ok) {
            const html = alertUser();
            const alert = document.querySelector('.row');
            alert.innerHTML = html;
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const option = document.createElement('option');
                option.value = conference.href;
                option.innerHTML = conference.name;
                selectTag.appendChild(option);
            }
            const conferenceSpinTag = document.getElementById("loading-conference-spinner");
            selectTag.classList.remove("d-none");
            conferenceSpinTag.classList.add("d-none");
        }
        const formTag = document.getElementById("create-attendee-form");
        formTag.addEventListener("submit", async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const newAttendeeUrl = "http://localhost:8001/api/attendees/";
            const fetchOptions = {
                method: 'post',
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(newAttendeeUrl, fetchOptions);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
            }
        });

    } catch (e) {
        console.log(e);
    }
});
