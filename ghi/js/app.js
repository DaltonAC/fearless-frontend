window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    function createCard(name, description, pictureUrl, startDate, endDate, location) {
        return `
        <div class="col">
          <div class="card shadow">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">${name}</h5>
              <small class="text-muted">${location}</small>
              <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
            ${startDate} - ${endDate}
            </div>
          </div>
        </div>
       <br>
        `;
      }

      function alertUser(response){
        return `
        <div class="alert alert-warning" role="alert">
            Can not get the conference data! Status Code: ${response}
        </div>
        `;
      }

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad -- ALERT HERE
        const html = alertUser(response.status)
        const alert = document.querySelector('.row');
        alert.innerHTML = html;

      } else {
        const data = await response.json();


        for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

            // Figure out what to do when the response is bad -- ALERT HERE
        if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts).toLocaleDateString();
            const endDate = new Date(details.conference.ends).toLocaleDateString();
            const location = details.conference.location.name
            const html = createCard(title, description, pictureUrl, startDate, endDate, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
        }
        }
      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error(e)
    }

  });
